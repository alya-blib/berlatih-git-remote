<?php
require_once('animal.php');
require('ape.php');
require('frog.php');


$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold-blooded : $sheep->cold_blooded <br> <br>"; // "no"

$sungokong = new Ape("kera sakti");


echo "Nama : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold-blooded : $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"
echo "<br> <br>";

$kodok = new Frog("buduk");


echo "Nama : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold-blooded : $kodok->cold_blooded <br>";
$kodok->jump(); // "hop hop"
?>